# Laser Red Neutrino
## An un-cluttered vagrant environment for developing Wordpress websites.

### Installation
1. Clone this repo into your project folder.
2. Edit the Vagrantfile to add a hostname.
3. `vagrant up`
4. On first boot, you will need to run `vagrant reload` in order for the hostname to be recognised on the network.
5. READ THE LINE ABOVE, EVERYONE FORGETS THIS. (You only need to do it once)


### Box includes
* PHP 5.5.9

* MySQL 5.5.44

* Puppet to install the latest Wordpress release.

* PhpMyAdmin

* xDebug for nice php errors and fancy var_dumps

* Avahi daemon for multicast DNS

### Yes the box is included in this repo
It isn't ideal, but hosting vagrant boxes is expensive.
